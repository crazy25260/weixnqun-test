
<?php if ($type=='article') { ?>
<title><?php if ($article->Metas->ydbaijia_articletitle) { ?><?php  echo $article->Metas->ydbaijia_articletitle;  ?>_<?php  echo $article->Category->Name;  ?>_<?php  echo $name;  ?><?php }else{  ?><?php  echo $title;  ?>_<?php  echo $article->Category->Name;  ?>_<?php  echo $name;  ?><?php } ?></title>
<?php 
$aryTags = array();
foreach($article->Tags as $key){$aryTags[] = $key->Name;}
if(count($aryTags)>0){$keywords = implode(',',$aryTags);} else {$keywords = $zbp->name;}
$description = trim(SubStrUTF8(TransferHTML($article->Content,'[nohtml]'),135)).'...';
 ?>
<meta name="keywords" content="<?php if ($article->Metas->ydbaijia_articlekeywords) { ?><?php  echo $article->Metas->ydbaijia_articlekeywords;  ?><?php }else{  ?><?php  echo $keywords;  ?><?php } ?>" />
<meta name="description" content="<?php if ($article->Metas->ydbaijia_articledescription) { ?><?php  echo $article->Metas->ydbaijia_articledescription;  ?><?php }else{  ?><?php  echo $description;  ?><?php } ?>" />
<meta name="author" content="<?php  echo $article->Author->StaticName;  ?>" />
<?php if ($article->Prev) { ?><link rel='prev' title='<?php  echo $article->Prev->Title;  ?>' href='<?php  echo $article->Prev->Url;  ?>'/><?php } ?>
<?php if ($article->Next) { ?><link rel='next' title='<?php  echo $article->Next->Title;  ?>' href='<?php  echo $article->Next->Url;  ?>'/><?php } ?>
<link rel="canonical" href="<?php  echo $article->Url;  ?>"/>
<link rel='shortlink' href='<?php  echo $article->Url;  ?>'/>
<?php }elseif($type=='page') {  ?>
<title><?php  echo $title;  ?>_<?php  echo $name;  ?>_<?php  echo $subname;  ?></title>
<meta name="keywords" content="<?php  echo $title;  ?>,<?php  echo $name;  ?>" />
<?php 
$description = trim(SubStrUTF8(TransferHTML($article->Content,'[nohtml]'),135)).'...';
 ?>
<meta name="description" content="<?php  echo $description;  ?>" />
<meta name="author" content="<?php  echo $article->Author->StaticName;  ?>" />
<?php }elseif($type=='index') {  ?>
<title><?php if ($zbp->Config('ydbaijia')->hometitle&&$page=='1') { ?><?php  echo $zbp->Config('ydbaijia')->hometitle;  ?><?php }else{  ?><?php  echo $name;  ?><?php if ($page>'1') { ?>_第<?php  echo $pagebar->PageNow;  ?>页<?php } ?>_<?php  echo $subname;  ?><?php } ?></title>
<?php if ($zbp->Config('ydbaijia')->homekeywords) { ?><meta name="Keywords" content="<?php  echo $zbp->Config('ydbaijia')->homekeywords;  ?>" /><?php } ?>
<?php if ($zbp->Config('ydbaijia')->homedescription) { ?><meta name="description" content="<?php  echo $zbp->Config('ydbaijia')->homedescription;  ?>" /><?php } ?>
<?php }elseif($type=='tag') {  ?>
<title><?php if ($tag->Metas->ydbaijia_tagtitle) { ?><?php  echo $tag->Metas->ydbaijia_tagtitle;  ?><?php if ($page>'1') { ?>_第<?php  echo $pagebar->PageNow;  ?>页<?php } ?><?php }else{  ?><?php  echo $tag->Name;  ?>_<?php  echo $name;  ?><?php if ($page>'1') { ?>_第<?php  echo $pagebar->PageNow;  ?>页<?php } ?>_<?php  echo $subname;  ?><?php } ?></title>
<meta name="Keywords" content="<?php if ($tag->Metas->ydbaijia_tagkeywords) { ?><?php  echo $tag->Metas->ydbaijia_tagkeywords;  ?><?php }else{  ?><?php  echo $tag->Name;  ?><?php } ?>">
<?php if ($tag->Intro || $tag->Metas->ydbaijia_tagdescription) { ?><meta name="description" content="<?php if ($tag->Metas->ydbaijia_tagdescription) { ?><?php  echo $tag->Metas->ydbaijia_tagdescription;  ?><?php }else{  ?><?php  echo $tag->Intro;  ?><?php } ?>"><?php } ?>
<?php }elseif($type=='category') {  ?>
<title><?php if ($category->Metas->ydbaijia_catetitle) { ?><?php  echo $category->Metas->ydbaijia_catetitle;  ?><?php if ($page>'1') { ?>_第<?php  echo $pagebar->PageNow;  ?>页<?php } ?><?php }else{  ?><?php  echo $title;  ?>_<?php  echo $name;  ?><?php } ?></title>
<meta name="Keywords" content="<?php if ($category->Metas->ydbaijia_catekeywords) { ?><?php  echo $category->Metas->ydbaijia_catekeywords;  ?><?php }else{  ?><?php  echo $title;  ?>,<?php  echo $name;  ?><?php } ?>" />
<meta name="description" content="<?php if ($category->Metas->ydbaijia_catedescription) { ?><?php  echo $category->Metas->ydbaijia_catedescription;  ?><?php }else{  ?><?php  echo $title;  ?>_<?php  echo $name;  ?><?php } ?>" />
<?php }else{  ?>
<title><?php  echo $title;  ?>_<?php  echo $name;  ?></title>
<meta name="Keywords" content="<?php  echo $title;  ?>,<?php  echo $name;  ?>" />
<meta name="description" content="<?php  echo $title;  ?>_<?php  echo $name;  ?><?php if ($page>'1') { ?>_当前是第<?php  echo $pagebar->PageNow;  ?>页<?php } ?>" />
<?php } ?>