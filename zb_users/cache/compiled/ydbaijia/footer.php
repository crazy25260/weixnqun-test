
<div id="footer">
	<div class="footer container">
		<?php if ($zbp->Config( 'ydbaijia' )->foot) { ?>
		<div class="fnav">
			<?php  echo $zbp->Config( 'ydbaijia' )->foothtml;  ?>
		</div>
		<?php } ?>
		<p><?php  echo $copyright;  ?><br>Powered By <?php  echo $zblogphpabbrhtml;  ?></p>
	</div>
</div>
<?php if ($zbp->Config( 'ydbaijia' )->gotop) { ?>
<div class="bottom_tools">
	<a id="scrollUp" href="javascript:;" title="返回顶部"><i class="fa fa-angle-up"></i></a> <?php if ($zbp->Config('ydbaijia')->goqrcode) { ?>
	<div class="qr_tool"><i class="fa fa-qrcode"></i>
	</div>
	<div class="qr_img"><img src="<?php if ($zbp->Config( 'ydbaijia' )->goqrcodeimg) { ?><?php  echo $zbp->Config( 'ydbaijia' )->goqrcodeimg;  ?><?php }else{  ?><?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/style/images/qr.jpg<?php } ?>"/>
	</div><?php } ?> <?php if ($type=='article'||$type=='page') { ?><?php if ($zbp->Config('ydbaijia')->gocomment) { ?><a class="topcomment" href="#comments" title="评论"><i class="fa fa-commenting"></i></a><?php } ?><?php } ?>
</div>
<?php } ?>
<script src="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/script/jq.tabslet.min.js"></script>
<?php if ($zbp->Config('ydbaijia')->asidegs) { ?>
<script src="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/script/ResizeSensor.min.js" type="text/javascript"></script>
<script src="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/script/theia-sticky-sidebar.min.js" type="text/javascript"></script>
<script>jQuery(document).ready(function($) {jQuery('.mainright').theiaStickySidebar({ additionalMarginTop: 10,});});</script>
<?php } ?>
<script src="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/script/common.js" type="text/javascript"></script>
<script src="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/script/custom.js" type="text/javascript"></script>
<?php if ($zbp->Config('ydbaijia')->wxjz) { ?><?php if ($type!=='article'&&$type!=='page') { ?><script type="text/javascript" src="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/script/wx.js"></script><?php } ?><?php } ?>
<?php if ($zbp->Config('ydbaijia')->slider) { ?>
<script src="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/script/swiper.jq.min.js" type="text/javascript"></script>
<script>
	var swiper = new Swiper( '.swiper-container', {
		loop: true,
		pagination: '.swiper-pagination',
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		paginationClickable: true,
		// spaceBetween: 30,
		centeredSlides: true,
		autoplay: 5500,
		autoplayDisableOnInteraction: false
	} );
</script>
<?php } ?>
<?php if ($type=='article'||$type=='page') { ?><?php if ($zbp->Config( 'ydbaijia' )->share) { ?>
<script type="text/javascript" src="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/script/jquery.qrcode.min.js"></script>
<script>
$("#code").qrcode({ 
    width: 110, //宽度 
    height:110, //高度 
    text: "<?php  echo $article->Url;  ?>" //任意内容 
});
</script>
<?php } ?><?php } ?>
<?php  echo $footer;  ?>
</body>
</html>