
<?php  include $this->GetTemplate('header');  ?>
<body>
<?php  include $this->GetTemplate('post-nav');  ?>
<div id="main">
	<div class="main container">
		<div class="mainleft">
			<div class="listpost">
				<ul>
					<?php  foreach ( $articles as $key=>$article) { ?><?php if ($zbp->CheckPlugin('IMAGE')) { ?><?php IMAGE::getPics($article,180,120,4) ?><?php } ?>
					<?php if ($article->IsTop) { ?><?php  include $this->GetTemplate('post-istop');  ?><?php }else{  ?><?php  include $this->GetTemplate('post-multi');  ?><?php } ?><?php }   ?>
				</ul>
				<?php  include $this->GetTemplate('pagebar');  ?>
			</div>
			
		</div>
		<?php  include $this->GetTemplate('post-sidebar');  ?>
	</div>
</div>
<?php  include $this->GetTemplate('footer');  ?>