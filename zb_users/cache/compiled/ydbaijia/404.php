<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>404 - 对不起，页面未找到</title>
<link href="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/style/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/style/css/swiper.min.css" rel="stylesheet">
<?php if ($zbp->Config('ydbaijia')->coloroff) { ?>
<link rel="stylesheet" type="text/css" href="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/style/style.ok.css" />
<?php }else{  ?>
<link rel="stylesheet" type="text/css" href="<?php  echo $host;  ?>zb_users/theme/<?php  echo $theme;  ?>/style/style.css" />
<?php } ?>
<script src="<?php  echo $host;  ?>zb_system/script/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="<?php  echo $host;  ?>zb_system/script/zblogphp.js" type="text/javascript"></script>
<script src="<?php  echo $host;  ?>zb_system/script/c_html_js_add.php" type="text/javascript"></script>
<?php if ($zbp->Config( 'ydbaijia' )->favicon) { ?>
<link rel="apple-touch-icon" href="<?php  echo $zbp->Config( 'ydbaijia' )->favicon;  ?>">
<link rel="shortcut icon" href="<?php  echo $zbp->Config( 'ydbaijia' )->favicon;  ?>" type="image/x-icon">
<link rel="icon" href="<?php  echo $zbp->Config( 'ydbaijia' )->favicon;  ?>" type="image/x-icon">
<?php } ?>
<?php if ($type=='index'&&$page=='1') { ?>
<link rel="alternate" type="application/rss+xml" href="<?php  echo $feedurl;  ?>" title="<?php  echo $name;  ?>" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?php  echo $host;  ?>zb_system/xml-rpc/?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?php  echo $host;  ?>zb_system/xml-rpc/wlwmanifest.xml" />
<?php } ?>
<?php if ($zbp->Config('ydbaijia')->head) { ?><?php  echo $zbp->Config('ydbaijia')->headhtml;  ?><?php } ?>
<?php  echo $header;  ?>
</head><body>
<?php  include $this->GetTemplate('post-nav');  ?>
<div id="main">
	<div class="main container">
		<div class="mainleft">
			<div class="post">
				<div class="page404">
					<h1>404!</h1>
					<h3>对不起，页面未找到</h3>
					<div class="serach404">
						<p>找不到内容？尝试下站内搜索吧！</p>
						<form name="search" method="get" action="<?php  echo $host;  ?>search.php?act=search">
							<input type="text" name="q" placeholder="输入关键词"/>
							<button type="submit" class="submit" value="搜索">搜索</button>
						</form>
					</div>
					<div class="goindex404"><a href="<?php  echo $host;  ?>" title="风水学">返回首页</a></div>
				</div>
			</div>
		</div>
		<?php  include $this->GetTemplate('post-sidebar');  ?>
	</div>
</div><?php  include $this->GetTemplate('footer');  ?>