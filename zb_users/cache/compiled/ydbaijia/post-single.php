<body>
<?php  include $this->GetTemplate('post-nav');  ?>
<div id="main">
	<div class="main container">
		<div class="mainleft">
			
			<div class="post">
				<h1><?php  echo $article->Title;  ?></h1>
				<?php if ($zbp->Config('ydbaijia')->postinfo) { ?>
				<div class="info">
					<a href="<?php  echo $article->Category->Url;  ?>"><?php  echo $article->Category->Name;  ?></a><i></i>
					<?php if ($zbp->Config( 'ydbaijia' )->timestyle=='1') { ?><?php  echo $article->Time('m-d');  ?><?php }else{  ?><?php  echo ydbaijia_TimeAgo($article->Time());  ?><?php } ?><i></i>
					阅读：<?php  echo $article->ViewNums;  ?><i></i>
					评论：<?php  echo $article->CommNums;  ?>

					<?php if ($user->ID>0) { ?><span><?php if ($type=='article'||$type=='page') { ?><a href="<?php  echo $host;  ?>zb_system/admin/edit.php?act=ArticleEdt&id=<?php  echo $article->ID;  ?>" target="_blank"><i class="fa fa-pencil-square-o"></i>编辑</a><?php } ?></span><?php } ?>
				</div>
				<?php } ?>
				<div class="article_content">
					<?php if (ydbaijia_is_mobile()) { ?>
					<?php if ($zbp->Config( 'ydbaijia' )->ad2off) { ?><?php  echo $zbp->Config( 'ydbaijia' )->mad2;  ?><?php } ?>
					<?php }else{  ?>
					<?php if ($zbp->Config( 'ydbaijia' )->ad2off) { ?><?php  echo $zbp->Config( 'ydbaijia' )->ad2;  ?><?php } ?>
					<?php } ?>
					<?php  echo $article->Content;  ?>
					<?php if (ydbaijia_is_mobile()) { ?>
					<?php if ($zbp->Config( 'ydbaijia' )->ad3off) { ?><?php  echo $zbp->Config( 'ydbaijia' )->mad3;  ?><?php } ?>
					<?php }else{  ?>
					<?php if ($zbp->Config( 'ydbaijia' )->ad3off) { ?><?php  echo $zbp->Config( 'ydbaijia' )->ad3;  ?><?php } ?>
					<?php } ?>
				</div>
				<?php if ($article->Tags) { ?>
				<div class="tags">
					标签：<?php  foreach ( $article->Tags as $tag) { ?><a href="<?php  echo $tag->Url;  ?>"><?php  echo $tag->Name;  ?></a><?php }   ?>
				</div>
				<?php } ?>
				<?php if ($zbp->Config( 'ydbaijia' )->copyrightoff) { ?>
				<div class="rights">
					<?php  echo $zbp->Config( 'ydbaijia' )->copyrighthtml;  ?>
				</div>
				<?php } ?>

				

				<?php if ($zbp->Config( 'ydbaijia' )->share) { ?>
				<div id="share">
					<div class="sharel">
						<p>分享：</p>
						<div class="bdsharebuttonbox">
							<a href="#" class="bds_weixin_icon" data-cmd="weixin" title="分享到微信"></a>
							<a href="#" class="bds_tsina_icon" data-cmd="tsina" title="分享到新浪微博"></a>
							<a href="#" class="bds_sqq_icon" data-cmd="sqq" title="分享到QQ好友"></a>
							<a href="#" class="bds_qzone_icon" data-cmd="qzone" title="分享到QQ空间"></a>
							<a href="#" class="bds_more_icon" data-cmd="more"></a>
						</div><script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
					</div>
					<div class="sharer">
						<i></i>
						<div class="qrimg">
							<div id="code"></div>
							<p>扫一扫在手机阅读、分享本文</p>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if ($zbp->Config('ydbaijia')->related) { ?>
				<div class="related">
					<h4>相关文章</h4>
					<ul class="<?php if ($zbp->Config('ydbaijia')->relatedstyle=='2') { ?>ul_t<?php }else{  ?>ul_img<?php } ?>">
						<?php  $aid=$article->ID;  ?>
<?php  $tagid=$article->Tags;  ?>
<?php  $cid=$article->Category->ID;  ?>
<?php 
$str = '';
$tagrd = array_rand( $tagid );
if ( sizeof( $tagid ) > 0 && ( $tagid[ $tagrd ]->Count ) > 1 ) {
$tagi = '%{' . $tagrd . '}%';
$where = array( array( '=', 'log_Status', '0' ), array( 'like', 'log_Tag', $tagi ), array( '<>', 'log_ID', $aid ) );
} else {
$where = array( array( '=', 'log_Status', '0' ), array( '=', 'log_CateID', $cid ), array( '<>', 'log_ID', $aid ) );
}
switch ( $zbp->option[ 'ZC_DATABASE_TYPE' ] ) {
case 'mysql':
case 'mysqli':
case 'pdo_mysql':
$order = array( 'RAND()' => '' );
break;
case 'sqlite':
case 'sqlite3':
$order = array( 'RANDOM()' => '' );
break;
}
if($zbp->Config('ydbaijia')->relatednum){
$num=$zbp->Config('ydbaijia')->relatednum;
}else{
$num=6;
}
$array = $zbp->GetArticleList( array( '*' ), $where, $order, array( $num ), '' );
 ?>
<?php  foreach ( $array as $related) { ?>
<?php if ($zbp->Config('ydbaijia')->relatedstyle=='2') { ?><li><a href="<?php  echo $related->Url;  ?>" target="_blank"><?php  echo $related->Title;  ?></a></li><?php }else{  ?><li><a href="<?php  echo $related->Url;  ?>" target="_blank"><div class="img"><img src="<?php if ($zbp->Config('ydbaijia')->thumb2) { ?><?php  echo ydbaijia_thumb2($related,225,120,0);  ?><?php }else{  ?><?php  echo ydbaijia_thumbnail($related);  ?><?php } ?>" alt="<?php  echo $related->Title;  ?>"></div><p><?php  echo $related->Title;  ?></p></a></li><?php } ?>
<?php }   ?>
					</ul>
				</div><?php } ?>
				<div class="Prev_Next">
				<?php if ($article->Prev) { ?><span><b>上一篇：</b><a href="<?php  echo $article->Prev->Url;  ?>"><?php  echo $article->Prev->Title;  ?></a></span><?php } ?>
				<?php if ($article->Next) { ?><span><b>下一篇：</b><a href="<?php  echo $article->Next->Url;  ?>"><?php  echo $article->Next->Title;  ?></a></span><?php } ?>
				</div>
				<?php if (!$article->IsLock) { ?><?php  include $this->GetTemplate('comments');  ?><?php } ?>
			</div>
		</div>
		<!--//-->
		<?php  include $this->GetTemplate('post-sidebar');  ?>
	</div>
</div>