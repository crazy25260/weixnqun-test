<body>
<?php  include $this->GetTemplate('post-nav');  ?>
<div id="main">
	<div class="main container">
		<div class="mainleft">
			
			<div class="post">
				<h1><?php  echo $article->Title;  ?></h1>
				<?php if ($user->ID>0) { ?>
				<div class="info">
					<span><?php if ($type=='article'||$type=='page') { ?><a href="<?php  echo $host;  ?>zb_system/admin/edit.php?act=ArticleEdt&id=<?php  echo $article->ID;  ?>" target="_blank"><i class="fa fa-pencil-square-o"></i>编辑</a><?php } ?></span>
				</div>
				<?php } ?>
				<div class="article_content">
					<?php if ($zbp->Config( 'ydbaijia' )->ad2off) { ?><?php  echo $zbp->Config( 'ydbaijia' )->ad2;  ?><?php } ?>
					<?php  echo $article->Content;  ?>
					<?php if ($zbp->Config( 'ydbaijia' )->ad3off) { ?><?php  echo $zbp->Config( 'ydbaijia' )->ad3;  ?><?php } ?>
				</div>
				<?php if ($zbp->Config( 'ydbaijia' )->copyrightoff) { ?>
				<div class="rights">
					<?php  echo $zbp->Config( 'ydbaijia' )->copyrighthtml;  ?>
				</div>
				<?php } ?>
				<?php if ($zbp->Config( 'ydbaijia' )->share) { ?>
				<div id="share">
					<div class="sharel">
						<p>分享：</p>
						<div class="bdsharebuttonbox">
							<a href="#" class="bds_weixin_icon" data-cmd="weixin" title="分享到微信"></a>
							<a href="#" class="bds_tsina_icon" data-cmd="tsina" title="分享到新浪微博"></a>
							<a href="#" class="bds_sqq_icon" data-cmd="sqq" title="分享到QQ好友"></a>
							<a href="#" class="bds_qzone_icon" data-cmd="qzone" title="分享到QQ空间"></a>
							<a href="#" class="bds_more_icon" data-cmd="more"></a>
						</div><script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
					</div>
					<div class="sharer">
						<i></i>
						<div class="qrimg">
							<div id="code"></div>
							<p>扫一扫在手机阅读、分享本文</p>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if (!$article->IsLock) { ?><?php  include $this->GetTemplate('comments');  ?><?php } ?>
			</div>
		</div>
		<!--//-->
		<?php  include $this->GetTemplate('post-sidebar');  ?>
	</div>
</div>