<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action='root';
if (!$zbp->CheckRights($action)) {$zbp->ShowError(6);die();}
if (!$zbp->CheckPlugin('custom_share')) {$zbp->ShowError(48);die();}

$blogtitle='custom_share';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
if(isset($_POST['Wechat'])){
    global $jssdk;
    foreach($_POST['Wechat'] as $key=>$val){
        $zbp->Config('CustomShare')->$key = $val;
    }
    $zbp->SaveConfig('CustomShare');
    $jssdk = new JSSDK($zbp->Config('CustomShare')->appId, $zbp->Config('CustomShare')->appSecret);   //这里改成你自己公众号里的
    $zbp->ShowHint('good');
}
?>
<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle;?></div>
  <div class="SubMenu">
  </div>
  <div id="divMain2">
<!--代码-->
      <form method="post">
          <table border="1" class="tableFull tableBorder">
              <tr>
                  <th>微信自定义分享的公众号配置项</th>
              </tr>
              <tr>
                  <td>公众号的appId：<input type="text"  name="Wechat[appId]" value="<?php echo $zbp->Config('CustomShare')->appId;?>"> </td>
              </tr>
              <tr>
                  <td>公众号的appSecret：<input type="text"  name="Wechat[appSecret]" value="<?php echo $zbp->Config('CustomShare')->appSecret;?>"> </td>
              </tr>
              <tr>
                  <td>文章加载超时时默认的分享标题：<input type="text"  name="Wechat[title]" value="<?php echo $zbp->Config('CustomShare')->title;?>"> </td>
              </tr>
              <tr>
                  <td>文章加载超时时默认的分享描述：<input type="text"  name="Wechat[desc]" value="<?php echo $zbp->Config('CustomShare')->desc;?>"> </td>
              </tr>
              <tr>
                  <td>文章加载超时时默认的分享图标地址：<input type="text"  name="Wechat[imgUrl]" value="<?php echo $zbp->Config('CustomShare')->imgUrl;?>"> 如：http://www.fortunelife.vip/thumb/thumb-61-180-120-0-20180210141933_59615.jpg</td>
              </tr>
          </table>
          <hr/>
          <p>
              <input type="submit" class="button" value="<?php echo $lang['msg']['submit'] ?>" />
          </p>
      </form>
  </div>
</div>

<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>