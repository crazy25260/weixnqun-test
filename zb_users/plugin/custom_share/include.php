<?php

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'include'.DIRECTORY_SEPARATOR.'custom.share.php';
#注册插件
RegisterPlugin("custom_share","ActivePlugin_custom_share");

function ActivePlugin_custom_share() {
    global $zbp;
    global $jssdk;

    if (!$zbp->Config('CustomShare')) {
        $zbp->Config('CustomShare')->appId = 'wxacdda9276fe41e6b';
        $zbp->Config('CustomShare')->appSecret = 'ba36dd00a2529333c8c7db223d414e40';
        $zbp->SaveConfig('CustomShare');
    }

    $jssdk = new JSSDK($zbp->Config('CustomShare')->appId, $zbp->Config('CustomShare')->appSecret);   //这里改成你自己公众号里的
    Add_Filter_Plugin('Filter_Plugin_ViewPost_Template','custom_share_Filter_Plugin_ViewPost_Template');
    Add_Filter_Plugin('Filter_Plugin_Zbp_MakeTemplatetags','custom_share_Zbp_MakeTemplatetags');
}

function InstallPlugin_custom_share() {
}

function UninstallPlugin_custom_share() {
}

$jssdk = null;
function custom_share_Filter_Plugin_ViewPost_Template(&$template){
    global $zbp;
    global $sharePackage;
    global $signPackage;
    global $jssdk;

    $article = $template->GetTags('article');
    $customShare = new CustomShare;
    $sharePackage = $customShare->getSharePackage($article);
    $signPackage = $jssdk->GetSignPackage();

    $s = <<<js
<script>wx.config({
        debug: true,
        appId: "{$signPackage['appId']}",
        timestamp: "{$signPackage['timestamp']}",
        nonceStr: "{$signPackage['nonceStr']}",
        signature: "{$signPackage['signature']}",
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone']
    });


    wx.ready(function () {
        var shareData = {
            title: "{$sharePackage['title']}",
            desc: "{$sharePackage['desc']}",
            link: window.location.href,
            imgUrl: "{$sharePackage['imgUrl']}",
            success: function () {
                // 用户确认分享后执行的回调函数
                alert('分享成功');
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
                //alert('取消了分享');
            }
        };

        wx.onMenuShareAppMessage(shareData);//分享给好友
        wx.onMenuShareTimeline(shareData);//分享到朋友圈
        wx.onMenuShareQQ(shareData);//分享给手机QQ
        wx.onMenuShareWeibo(shareData);//分享腾讯微博
        wx.onMenuShareQZone(shareData);//分享到QQ空间
    });

    wx.error(function (res) {
        //alert(res.errMsg);//错误提示
    });</script>
js;
    $zbp->footer .=  $s."\r\n";
}

function custom_share_Zbp_MakeTemplatetags() {
    global $zbp;
    $s = <<<js
<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js" type="text/javascript"></script>
js;
    $zbp->header .=  $s."\r\n";
}