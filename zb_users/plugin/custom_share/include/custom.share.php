<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2018/3/9
 * Time: 13:08
 */

class CustomShare
{
    public function getSharePackage($article) {
        global $zbp;

        // 注意 URL 一定要动态获取，不能 hardcode.
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $currentUrl = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $imgUrl = "http://www.fortunelife.vip/thumb/thumb-61-180-120-0-20180210141933_59615.jpg";

        $sharePackage = array();
        if($zbp->Config('CustomShare')){
            $sharePackage['title'] = $zbp->Config('CustomShare')->title;
            $sharePackage['desc'] = $zbp->Config('CustomShare')->desc;
            $sharePackage['imgUrl'] = $zbp->Config('CustomShare')->imgUrl;
        } else{
            $sharePackage['title'] = "财富生活网文章";
            $sharePackage['desc'] = "这是一篇来自财富生活网的文章";
            $sharePackage['link'] = $currentUrl;
            $sharePackage['imgUrl'] = $imgUrl;
        }

        $sharePackage['title'] = $article->Title;
        $sharePackage['desc'] = $article->Intro;
        $sharePackage['imgUrl'] = ydbaijia_thumb2($article,180,120,0);

//        $pattern = '/<img.*?src="(.*?)(?=")/';
//        $content = $article->Content;
//        preg_match_all($pattern, $content, $matchContent);
//        //判断数量用
//        $arr = array();
//        if (isset($matchContent[1][0])) {
//            foreach ($matchContent[1] as $url) {
//                $arr[] = $url;
//            }
//        }
//        $article->thumb2 = $arr;
//        $article->thumb2_count = count($arr);
//        if ($article->thumb2_count > 0) {
//            $sharePackage['imgUrl'] = $arr[0];
//        }
        return $sharePackage;
    }
}

//added by zhangrui @20190210 --begin
class JSSDK
{
    private $appId;
    private $appSecret;

    public function __construct($appId, $appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }

    public function getSignPackage()
    {
        $jsapiTicket = $this->getJsApiTicket();
        // 注意 URL 一定要动态获取，不能 hardcode.
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $timestamp = time();
        $nonceStr = $this->createNonceStr();

        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

        $signature = sha1($string);

//        $signPackage = new SignPackage();
//        $signPackage->setAppId($this->appId);
//        $signPackage->setNoceStr($nonceStr);
//        $signPackage->setTimestamp($timestamp);
//        $signPackage->setUrl($url);
//        $signPackage->setSignature($signature);
//        $signPackage->setRawString($string);

        $signPackage = array();
        $signPackage['appId'] = $this->appId;
        $signPackage['timestamp'] = $timestamp;
        $signPackage['nonceStr'] = $nonceStr;
        $signPackage['signature'] = $signature;

        return $signPackage;
    }

    private function createNonceStr($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    public function getJsApiTicket()
    {
        // jsapi_ticket 应该全局存储与更新，以下代码以写入到文件中做示例
        $fileData = $this->get_php_file("jsapi_ticket.php");
        if (false != $fileData) {
            $data = json_decode($fileData);
        }

        //$data = json_decode($this->get_php_file("jsapi_ticket.php"));
        if (false == $data || $data->expire_time < time()) {
            $accessToken = $this->getAccessToken();
            // 如果是企业号用以下 URL 获取 ticket
            // $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
            $res = json_decode($this->httpGet($url));
            if (false == $data) {
                $data = $res;
            }
            $ticket = $res->ticket;
            if ($ticket) {
                $data->expire_time = time() + 7000;
                $data->jsapi_ticket = $ticket;
                $this->set_php_file("jsapi_ticket.php", json_encode($data));
            }
        } else {
            $ticket = $data->jsapi_ticket;
        }

        return $ticket;
    }

    private function getAccessToken()
    {
        // access_token 应该全局存储与更新，以下代码以写入到文件中做示例
        $fileData = $this->get_php_file("access_token.php");
        if (false != $fileData) {
            $data = json_decode($fileData);
        }

        if (false == $data || $data->expire_time < time()) {
            // 如果是企业号用以下URL获取access_token
            // $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
            $res = json_decode($this->httpGet($url));
            $access_token = $res->access_token;
            if (false == $data) {
                $data = $res;
            }
            if ($access_token) {
                $data->expire_time = time() + 7000;
                $data->access_token = $access_token;
                $this->set_php_file("access_token.php", json_encode($data));
            }
        } else {
            $access_token = $data->access_token;
        }
        return $access_token;
    }

    private function httpGet($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        // 为保证第三方服务器与微信服务器之间数据传输的安全性，所有微信接口采用https方式调用，必须使用下面2行代码打开ssl安全校验。
        // 如果在部署过程中代码在此处验证失败，请到 http://curl.haxx.se/ca/cacert.pem 下载新的证书判别文件。
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($curl, CURLOPT_URL, $url);

        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }

    private function get_php_file($filename)
    {
        if (!file_exists($filename)) {
            return false;
        }

        $content = file_get_contents($filename);
        if ($content == false) {
            return false;
        }

        return trim(substr($content, 15));
    }

    private function set_php_file($filename, $content)
    {
        $fp = fopen($filename, "w");
        fwrite($fp, "<?php exit();?>" . $content);
        fclose($fp);
    }
}