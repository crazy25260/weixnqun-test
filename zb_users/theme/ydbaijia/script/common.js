/*
Theme ID: ydbaijia
Author: 老白
Author QQ：525887672
Author Email：525887672@qq.com
Author URI: http://www.ylefu.com/
*/
//PC搜索
$(function(){
	$("#search").click(function(){
		$(".search").slideToggle(100);
		$("#search i").toggleClass("fa-remove");
		$("#search i").toggleClass("fa-search");
//		$("#nav").slideUp(100);
	})
});
//移动端下拉
$(function(){
	$("#mnav").click(function(){
		$("#mnav i").toggleClass("fa-remove");
		$("#mnav i").toggleClass("fa-bars");
		$("#nav").slideToggle(100);
		$(".search").slideUp(100);
	})
});
//菜单跟随
$(function () {
	var nav=$(".topmenu"); //得到导航对象
	var win=$(window); //得到窗口对象
	var sc=$(document);//得到document文档对象。
	win.scroll(function () {
		if (sc.scrollTop() >= 180) {
			nav.addClass("fixednav");
//			$(".navTmp").fadeIn();
		} else {
			nav.removeClass("fixednav");
//			$(".navTmp").fadeOut();
		}
	})
});
//迷你头部搜索
$(function(){
	$("#minisearch").click(function(){
		$(".minisearch").slideToggle(100);
	})
})
//侧栏相关文章跟随
$(function(){
    var $main         = $('.mainright'),
        mainHeight    = $main.height(),
        $fixBox       = $('#side_relates'),//
        subHeight     = $('#side_relates').height(),//
        winHeight     = $(window).height(), //窗口大小
        mainBottomPos = $main.offset().top + mainHeight,
        maxTop        = mainHeight - $fixBox.height() - (subHeight-$fixBox.height()),
        threshold     = maxTop;
	
    $(window).scroll(function(){
        
        var scrollTop = $(document.body).scrollTop(),
            delta = mainBottomPos - winHeight - scrollTop;
        
        if(delta <= 0){
        
            $fixBox.addClass('fix-btm');
            $fixBox.css('top',maxTop);
            
        }else if(delta <= threshold){
        
            $fixBox.removeClass('fix-btm');
            $fixBox.css('top','auto');
            if(!$fixBox.hasClass('fix')){
                $fixBox.addClass('fix');
            }
            
        }else{
            $fixBox.removeClass('fix-btm');
            $fixBox.removeClass('fix');
        }
        
    });

});

//图标区二维码
$(function () {
	$("#lbicon-wechat").hover(function(){
		$(".lbicon-wechat").slideToggle(100);
	})
});
//登陆后
$(function () {
	$("#admin").click(function () {
		$(".admin").slideToggle(50);
	})
});
//评论区
$(function () {
	$(".textarea").click(function () {
		$(".com-info").slideToggle(100);
	})
});
//高亮
$(function () {
	var datatype = $("#monavber").attr("data-type");
	$(".navbar>li ").each(function () {
		try {
			var myid = $(this).attr("id");
			if ("index" == datatype) {
				if (myid == "nvabar-item-index") {
					$("#nvabar-item-index").addClass("active");
				}
			} else if ("category" == datatype) {
				var infoid = $("#monavber").attr("data-infoid");
				if (infoid != null) {
					var b = infoid.split(' ');
					for (var i = 0; i < b.length; i++) {
						if (myid == "navbar-category-" + b[i]) {
							$("#navbar-category-" + b[i] + "").addClass("active");
						}
					}
				}
			} else if ("article" == datatype) {
				var infoid = $("#monavber").attr("data-infoid");
				if (infoid != null) {
					var b = infoid.split(' ');
					for (var i = 0; i < b.length; i++) {
						if (myid == "navbar-category-" + b[i]) {
							$("#navbar-category-" + b[i] + "").addClass("active");
						}
					}
				}
			} else if ("page" == datatype) {
				var infoid = $("#monavber").attr("data-infoid");
				if (infoid != null) {
					if (myid == "navbar-page-" + infoid) {
						$("#navbar-page-" + infoid + "").addClass("active");
					}
				}
			} else if ("tag" == datatype) {
				var infoid = $("#monavber").attr("data-infoid");
				if (infoid != null) {
					if (myid == "navbar-tag-" + infoid) {
						$("#navbar-tag-" + infoid + "").addClass("active");
					}
				}
			}
		} catch (E) {}
	});
	$("#monavber").delegate("a", "click", function () {
		$(".navbar>li").each(function () {
			$(this).removeClass("active");
		});
		if ($(this).closest("ul") != null && $(this).closest("ul").length != 0) {
			if ($(this).closest("ul").attr("id") == "munavber") {
				$(this).addClass("active");
			} else {
				$(this).closest("ul").closest("li").addClass("active");
			}
		}
	});
});
//返回顶部
$(function(){
 var $bottomTools = $('.bottom_tools');
 var $qrTools = $('.qr_tool');
 var qrImg = $('.qr_img');
 
 $(window).scroll(function () {
  var scrollHeight = $(document).height();
  var scrollTop = $(window).scrollTop();
  var $windowHeight = $(window).innerHeight();
  scrollTop > 50 ? $("#scrollUp").fadeIn(200).css("display","block") : $("#scrollUp").fadeOut(200);   
  $bottomTools.css("bottom", scrollHeight - scrollTop > $windowHeight ? 40 : $windowHeight + scrollTop + 40 - scrollHeight);
 });
 
 $('#scrollUp').click(function (e) {
  e.preventDefault();
  $('html,body').animate({ scrollTop:0});
 });

 $qrTools.hover(function () {
  qrImg.fadeIn();
 }, function(){
   qrImg.fadeOut();
 });
 
});
//视频自适应
function video_ok(){
	$('.article_content embed, .article_content video, .article_content iframe').each(function(){
		var w = $(this).attr('width'),
			h = $(this).attr('height')
		if( h ){
			$(this).css('height', $(this).width()/(w/h))
		}
	})
}
//文章图片自适应，自适应CSS宽度需设置为width:100%
$(function(){
	$(".article_content").find("img").css({ //去除style="width:;height:;"
		"width" : "",
		"height" : ""
	});
});
function img_ok(){
	$('.article_content img').each(function(){
		var w = $(this).attr('width'),
			h = $(this).attr('height')
		if( h ){
			$(this).css('height', $(this).width()/(w/h))
		}
	});
}
