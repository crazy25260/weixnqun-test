<?php /* EL PSY CONGROO */    		 					
require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions/Add_Filter_Plugin.php';      	 	   
require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions/Common.php';     	    	 
require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions/RegBuildModule.php';     					 	
require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions/thumb2/Thumb.php';    	  		  	
      	 		 	
RegisterPlugin("ydbaijia","ActivePlugin_ydbaijia");    	 			   
function ActivePlugin_ydbaijia(){       	   	
	global $zbp;        		  
	Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu','ydbaijia_AddMenu');//主题配置选项     		 			 
	Add_Filter_Plugin('Filter_Plugin_Zbp_BuildModule','ydbaijia_rebuild_Main');//重新加载边栏    		 		 	 
	if ($zbp->Config('ydbaijia')->seo=="true"){     		   	 
	Add_Filter_Plugin('Filter_Plugin_Category_Edit_Response','ydbaijia_cate_seo');//分类自定义字段    			 			 
	Add_Filter_Plugin('Filter_Plugin_Tag_Edit_Response','ydbaijia_tag_seo');//tag自定义字段    			  	  
	Add_Filter_Plugin('Filter_Plugin_Edit_Response5','ydbaijia_article_seo');    	 	  	  
	}    	 	  			
	Add_Filter_Plugin('Filter_Plugin_ViewList_Template','ydbaijia_tags_set');//hdp      	 	  	
}    				 	  
     		 			 
function ydbaijia_AddMenu(&$m){    	 	  	  
	global $zbp;    	 		   	
	array_unshift($m, MakeTopMenu("root",'主题配置',$zbp->host . "zb_users/theme/$zbp->theme/admin/relevant.php","","topmenu_ydbaijia"));    		  	  	
}       	 	  
    						  
function ydbaijia_SubMenu($id){        			 
	$arySubMenu = array(		    	   		  
		0 => array('主题说明', 'relevant', 'left', false),      	   		
	    1 => array('基本设置', 'config', 'left', false),    		 	 	 	
		3 => array('首页设置', 'index', 'left', false),     	   		 
		//8 => array('分类页', 'category', 'left', false),      			  	
		//4 => array('文章页', 'article', 'left', false),    	 		  		
		6 => array('导航菜单', 'nav', 'left', false),       		   
		7 => array('幻灯片', 'slide', 'left', false),    		   		 
		2 => array('SEO', 'seo', 'left', false),     	  	  	
		//9 => array('特有', 'special', 'left', false),           	
		11 => array('主题配色', 'color', 'left', false),      	  		 
		12 => array('缩略图', 'thumb2', 'left', false),        	   
		10 => array('页脚', 'foot', 'left', false),    							 
		5 => array('广告', 'ad', 'left', false),     	 		  	
		    	  	  	 
		    		  	   
	);        		 	
	foreach($arySubMenu as $k => $v){    				 	  
		echo '<a href="'.$v[1].'.php" '.($v[3]==true?'target="_blank"':'').'><span class="m-'.$v[2].' '.($id==$k?'m-now':'').'">'.$v[0].'</span></a>';    				  	 
	}      	     
}    						  
      				  
function InstallPlugin_ydbaijia(){     		 	 	 
	global $zbp;       					
	if(!$zbp->Config('ydbaijia')->HasKey('Version')){    	 	 		  
		$zbp->Config('ydbaijia')->Version = '1.0';    	 		 		 
		$zbp->Config('ydbaijia')->timestyle = '1';    	     		
		$zbp->Config('ydbaijia')->copyrightoff = 'true';
		$zbp->Config('ydbaijia')->copyrighthtml = '<h5>版权声明</h5>
<p>本文仅代表作者观点，不代表百度立场。<br>本文系作者授权百度百家发表，未经许可，不得转载。</p>';
		$zbp->Config('ydbaijia')->listinfo = 'true';      	 			 
		$zbp->Config('ydbaijia')->postinfo = 'true';     		 				
		$zbp->Config('ydbaijia')->login = 'true';    	 	 			 
		$zbp->Config('ydbaijia')->zcurl = '#';    	  	    
		$zbp->Config('ydbaijia')->tgurl = '#';     		 	  	
		$zbp->Config('ydbaijia')->share = 'true';    	     		
		$zbp->Config('ydbaijia')->related = 'true';    	   			 
		$zbp->Config('ydbaijia')->relatedstyle = '1';            
		$zbp->Config('ydbaijia')->relatednum = '6';     	   			
		$zbp->Config('ydbaijia')->target = 'true';    	   	  	
		$zbp->Config('ydbaijia')->gotop = 'true';       		  	
		$zbp->Config('ydbaijia')->sliderstyle = '1';    	 	     
		$zbp->Config('ydbaijia')->hdpid ='4,5,6';     		 	 	 
		$zbp->Config('ydbaijia')->tuijianid = '3,4,5';      	 				
		$zbp->Config('ydbaijia')->navbar = 'true';
		$zbp->Config('ydbaijia')->navhtml = '<li id="nvabar-item-index"><a href="http://www.ylefu.com/">首页</a></li>
<li id="navbar-category-16"><a href="http://www.ylefu.com/zblogzhizuo/">zblog模板制作</a></li>
<li id="navbar-category-17"><a href="http://www.ylefu.com/zblogphp/">zblogphp主题模板</a></li>
<li id="navbar-category-18"><a href="http://www.ylefu.com/zblogfree/">zblog免费模板</a></li>
<li id="navbar-category-28"><a href="http://www.ylefu.com/qianduan/">前端技术</a></li>';
		$zbp->Config('ydbaijia')->seo = 'true';    	  	 			
		$zbp->Config('ydbaijia')->hometitle = '首页标题请到主题配置内 - SEO项修改 - 老白zblog模板';       	 		 
		$zbp->Config('ydbaijia')->color = '#F2F2F2';    			   		
		$zbp->Config('ydbaijia')->color2 = '#0047FF';      	  		 
		$zbp->Config('ydbaijia')->color3 = '#FF6360';     			 		 
		$zbp->Config('ydbaijia')->thumb2 = 'true';    		    	 
		$zbp->Config('ydbaijia')->noimgstyle2 = '2';     			 	 	
		$zbp->Config('ydbaijia')->foot = 'true';
		$zbp->Config('ydbaijia')->foothtml = '<a href="http://www.ylefu.com/" target="_blank">zblogPHP模板</a>
<a href="http://www.ylefu.com/" target="_blank">zblogPHP模板</a>
<a href="http://www.ylefu.com/" target="_blank">zblogPHP模板</a>
<a href="http://www.ylefu.com/" target="_blank">zblogPHP模板</a>
<a href="http://www.ylefu.com/" target="_blank">zblogPHP模板</a>
<a href="http://www.ylefu.com/" target="_blank">zblogPHP模板</a>';
		$zbp->Config('ydbaijia')->ad1 = '<img src="图片地址" >';       				 
		$zbp->Config('ydbaijia')->ad2 = '<a href="链接网址"><img src="图片地址" ></a>';    	  		   
		$zbp->Config('ydbaijia')->ad3 = '<a href="链接网址">文字文字文字文字</a>';    	 		 	 	
		$zbp->Config('ydbaijia')->ad4 = '<a href="链接网址"><img src="图片地址" ></a>';       	   	
		$zbp->Config('ydbaijia')->ydbaijia_clearSetting ='';//清配置     		 		  
		$zbp->SaveConfig('ydbaijia');     		  		 
	}    		 			 	
}    								
    	    			
function UninstallPlugin_ydbaijia(){       	  		
	global $zbp;     	 		  	
	//清空主题配置    	 	 	 		
	if ($zbp->Config('ydbaijia')->ydbaijia_clearSetting){     			 		 
		$zbp->DelConfig('ydbaijia');		      		  	 
	}     		   		
	$zbp->SetHint('good','OK');    		  			 
}    		 					
     	 		   
      	 	   
?>