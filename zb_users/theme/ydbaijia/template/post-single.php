<?php echo'404';die();?><body>
{template:post-nav}
<div id="main">
	<div class="main container">
		<div class="mainleft">
			
			<div class="post">
				<h1>{$article.Title}</h1>
				{if $zbp->Config('ydbaijia')->postinfo}
				<div class="info">
					<a href="{$article.Category.Url}">{$article.Category.Name}</a><i></i>
					{if $zbp->Config( 'ydbaijia' )->timestyle=='1'}{$article.Time('m-d')}{else}{ydbaijia_TimeAgo($article.Time())}{/if}<i></i>
					阅读：{$article.ViewNums}<i></i>
					评论：{$article.CommNums}

					{if $user.ID>0}<span>{if $type=='article'||$type=='page'}<a href="{$host}zb_system/admin/edit.php?act=ArticleEdt&id={$article.ID}" target="_blank"><i class="fa fa-pencil-square-o"></i>编辑</a>{/if}</span>{/if}
				</div>
				{/if}
				<div class="article_content">
					{if ydbaijia_is_mobile()}
					{if $zbp->Config( 'ydbaijia' )->ad2off}{$zbp->Config( 'ydbaijia' )->mad2}{/if}
					{else}
					{if $zbp->Config( 'ydbaijia' )->ad2off}{$zbp->Config( 'ydbaijia' )->ad2}{/if}
					{/if}
					{$article.Content}
					{if ydbaijia_is_mobile()}
					{if $zbp->Config( 'ydbaijia' )->ad3off}{$zbp->Config( 'ydbaijia' )->mad3}{/if}
					{else}
					{if $zbp->Config( 'ydbaijia' )->ad3off}{$zbp->Config( 'ydbaijia' )->ad3}{/if}
					{/if}
				</div>
				{if $article.Tags}
				<div class="tags">
					标签：{foreach $article.Tags as $tag}<a href="{$tag.Url}">{$tag.Name}</a>{/foreach}
				</div>
				{/if}
				{if $zbp->Config( 'ydbaijia' )->copyrightoff}
				<div class="rights">
					{$zbp->Config( 'ydbaijia' )->copyrighthtml}
				</div>
				{/if}

				

				{if $zbp->Config( 'ydbaijia' )->share}
				<div id="share">
					<div class="sharel">
						<p>分享：</p>
						<div class="bdsharebuttonbox">
							<a href="#" class="bds_weixin_icon" data-cmd="weixin" title="分享到微信"></a>
							<a href="#" class="bds_tsina_icon" data-cmd="tsina" title="分享到新浪微博"></a>
							<a href="#" class="bds_sqq_icon" data-cmd="sqq" title="分享到QQ好友"></a>
							<a href="#" class="bds_qzone_icon" data-cmd="qzone" title="分享到QQ空间"></a>
							<a href="#" class="bds_more_icon" data-cmd="more"></a>
						</div><script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
					</div>
					<div class="sharer">
						<i></i>
						<div class="qrimg">
							<div id="code"></div>
							<p>扫一扫在手机阅读、分享本文</p>
						</div>
					</div>
				</div>
				{/if}
				{if $zbp->Config('ydbaijia')->related}
				<div class="related">
					<h4>相关文章</h4>
					<ul class="{if $zbp->Config('ydbaijia')->relatedstyle=='2'}ul_t{else}ul_img{/if}">
						{$aid=$article.ID}
{$tagid=$article.Tags}
{$cid=$article.Category.ID}
{php}
$str = '';
$tagrd = array_rand( $tagid );
if ( sizeof( $tagid ) > 0 && ( $tagid[ $tagrd ]->Count ) > 1 ) {
$tagi = '%{' . $tagrd . '}%';
$where = array( array( '=', 'log_Status', '0' ), array( 'like', 'log_Tag', $tagi ), array( '<>', 'log_ID', $aid ) );
} else {
$where = array( array( '=', 'log_Status', '0' ), array( '=', 'log_CateID', $cid ), array( '<>', 'log_ID', $aid ) );
}
switch ( $zbp->option[ 'ZC_DATABASE_TYPE' ] ) {
case 'mysql':
case 'mysqli':
case 'pdo_mysql':
$order = array( 'RAND()' => '' );
break;
case 'sqlite':
case 'sqlite3':
$order = array( 'RANDOM()' => '' );
break;
}
if($zbp->Config('ydbaijia')->relatednum){
$num=$zbp->Config('ydbaijia')->relatednum;
}else{
$num=6;
}
$array = $zbp->GetArticleList( array( '*' ), $where, $order, array( $num ), '' );
{/php}
{foreach $array as $related}
{if $zbp->Config('ydbaijia')->relatedstyle=='2'}<li><a href="{$related.Url}" target="_blank">{$related.Title}</a></li>{else}<li><a href="{$related.Url}" target="_blank"><div class="img"><img src="{if $zbp->Config('ydbaijia')->thumb2}{ydbaijia_thumb2($related,225,120,0)}{else}{ydbaijia_thumbnail($related)}{/if}" alt="{$related.Title}"></div><p>{$related.Title}</p></a></li>{/if}
{/foreach}
					</ul>
				</div>{/if}
				<div class="Prev_Next">
				{if $article.Prev}<span><b>上一篇：</b><a href="{$article.Prev.Url}">{$article.Prev.Title}</a></span>{/if}
				{if $article.Next}<span><b>下一篇：</b><a href="{$article.Next.Url}">{$article.Next.Title}</a></span>{/if}
				</div>
				{if !$article.IsLock}{template:comments}{/if}
			</div>
		</div>
		<!--//-->
		{template:post-sidebar}
	</div>
</div>