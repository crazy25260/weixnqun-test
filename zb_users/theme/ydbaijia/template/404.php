<?php echo'404';die();?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>404 - 对不起，页面未找到</title>
<link href="{$host}zb_users/theme/{$theme}/style/css/font-awesome.min.css" rel="stylesheet">
<link href="{$host}zb_users/theme/{$theme}/style/css/swiper.min.css" rel="stylesheet">
{if $zbp->Config('ydbaijia')->coloroff}
<link rel="stylesheet" type="text/css" href="{$host}zb_users/theme/{$theme}/style/style.ok.css" />
{else}
<link rel="stylesheet" type="text/css" href="{$host}zb_users/theme/{$theme}/style/style.css" />
{/if}
<script src="{$host}zb_system/script/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="{$host}zb_system/script/zblogphp.js" type="text/javascript"></script>
<script src="{$host}zb_system/script/c_html_js_add.php" type="text/javascript"></script>
{if $zbp->Config( 'ydbaijia' )->favicon}
<link rel="apple-touch-icon" href="{$zbp->Config( 'ydbaijia' )->favicon}">
<link rel="shortcut icon" href="{$zbp->Config( 'ydbaijia' )->favicon}" type="image/x-icon">
<link rel="icon" href="{$zbp->Config( 'ydbaijia' )->favicon}" type="image/x-icon">
{/if}
{if $type=='index'&&$page=='1'}
<link rel="alternate" type="application/rss+xml" href="{$feedurl}" title="{$name}" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="{$host}zb_system/xml-rpc/?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{$host}zb_system/xml-rpc/wlwmanifest.xml" />
{/if}
{if $zbp->Config('ydbaijia')->head}{$zbp->Config('ydbaijia')->headhtml}{/if}
{$header}
</head><body>
{template:post-nav}
<div id="main">
	<div class="main container">
		<div class="mainleft">
			<div class="post">
				<div class="page404">
					<h1>404!</h1>
					<h3>对不起，页面未找到</h3>
					<div class="serach404">
						<p>找不到内容？尝试下站内搜索吧！</p>
						<form name="search" method="get" action="{$host}search.php?act=search">
							<input type="text" name="q" placeholder="输入关键词"/>
							<button type="submit" class="submit" value="搜索">搜索</button>
						</form>
					</div>
					<div class="goindex404"><a href="{$host}" title="风水学">返回首页</a></div>
				</div>
			</div>
		</div>
		{template:post-sidebar}
	</div>
</div>{template:footer}