<?php echo'404';die();?>
<div id="footer">
	<div class="footer container">
		{if $zbp->Config( 'ydbaijia' )->foot}
		<div class="fnav">
			{$zbp->Config( 'ydbaijia' )->foothtml}
		</div>
		{/if}
		<p>{$copyright}<br>Powered By {$zblogphpabbrhtml}</p>
	</div>
</div>
{if $zbp->Config( 'ydbaijia' )->gotop}
<div class="bottom_tools">
	<a id="scrollUp" href="javascript:;" title="返回顶部"><i class="fa fa-angle-up"></i></a> {if $zbp->Config('ydbaijia')->goqrcode}
	<div class="qr_tool"><i class="fa fa-qrcode"></i>
	</div>
	<div class="qr_img"><img src="{if $zbp->Config( 'ydbaijia' )->goqrcodeimg}{$zbp->Config( 'ydbaijia' )->goqrcodeimg}{else}{$host}zb_users/theme/{$theme}/style/images/qr.jpg{/if}"/>
	</div>{/if} {if $type=='article'||$type=='page'}{if $zbp->Config('ydbaijia')->gocomment}<a class="topcomment" href="#comments" title="评论"><i class="fa fa-commenting"></i></a>{/if}{/if}
</div>
{/if}
<script src="{$host}zb_users/theme/{$theme}/script/jq.tabslet.min.js"></script>
{if $zbp->Config('ydbaijia')->asidegs}
<script src="{$host}zb_users/theme/{$theme}/script/ResizeSensor.min.js" type="text/javascript"></script>
<script src="{$host}zb_users/theme/{$theme}/script/theia-sticky-sidebar.min.js" type="text/javascript"></script>
<script>jQuery(document).ready(function($) {jQuery('.mainright').theiaStickySidebar({ additionalMarginTop: 10,});});</script>
{/if}
<script src="{$host}zb_users/theme/{$theme}/script/common.js" type="text/javascript"></script>
<script src="{$host}zb_users/theme/{$theme}/script/custom.js" type="text/javascript"></script>
{if $zbp->Config('ydbaijia')->wxjz}{if $type!=='article'&&$type!=='page'}<script type="text/javascript" src="{$host}zb_users/theme/{$theme}/script/wx.js"></script>{/if}{/if}
{if $zbp->Config('ydbaijia')->slider}
<script src="{$host}zb_users/theme/{$theme}/script/swiper.jq.min.js" type="text/javascript"></script>
<script>
	var swiper = new Swiper( '.swiper-container', {
		loop: true,
		pagination: '.swiper-pagination',
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		paginationClickable: true,
		// spaceBetween: 30,
		centeredSlides: true,
		autoplay: 5500,
		autoplayDisableOnInteraction: false
	} );
</script>
{/if}
{if $type=='article'||$type=='page'}{if $zbp->Config( 'ydbaijia' )->share}
<script type="text/javascript" src="{$host}zb_users/theme/{$theme}/script/jquery.qrcode.min.js"></script>
<script>
$("#code").qrcode({ 
    width: 110, //宽度 
    height:110, //高度 
    text: "{$article.Url}" //任意内容 
});
</script>
{/if}{/if}
{$footer}
</body>
</html>