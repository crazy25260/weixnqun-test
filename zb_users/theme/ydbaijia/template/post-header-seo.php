<?php echo'404';die();?>
{if $type=='article'}
<title>{if $article->Metas->ydbaijia_articletitle}{$article->Metas->ydbaijia_articletitle}_{$article.Category.Name}_{$name}{else}{$title}_{$article.Category.Name}_{$name}{/if}</title>
{php}
$aryTags = array();
foreach($article->Tags as $key){$aryTags[] = $key->Name;}
if(count($aryTags)>0){$keywords = implode(',',$aryTags);} else {$keywords = $zbp->name;}
$description = trim(SubStrUTF8(TransferHTML($article->Content,'[nohtml]'),135)).'...';
{/php}
<meta name="keywords" content="{if $article->Metas->ydbaijia_articlekeywords}{$article->Metas->ydbaijia_articlekeywords}{else}{$keywords}{/if}" />
<meta name="description" content="{if $article->Metas->ydbaijia_articledescription}{$article->Metas->ydbaijia_articledescription}{else}{$description}{/if}" />
<meta name="author" content="{$article.Author.StaticName}" />
{if $article.Prev}<link rel='prev' title='{$article.Prev.Title}' href='{$article.Prev.Url}'/>{/if}
{if $article.Next}<link rel='next' title='{$article.Next.Title}' href='{$article.Next.Url}'/>{/if}
<link rel="canonical" href="{$article.Url}"/>
<link rel='shortlink' href='{$article.Url}'/>
{elseif $type=='page'}
<title>{$title}_{$name}_{$subname}</title>
<meta name="keywords" content="{$title},{$name}" />
{php}
$description = trim(SubStrUTF8(TransferHTML($article->Content,'[nohtml]'),135)).'...';
{/php}
<meta name="description" content="{$description}" />
<meta name="author" content="{$article.Author.StaticName}" />
{elseif $type=='index'}
<title>{if $zbp->Config('ydbaijia')->hometitle&&$page=='1'}{$zbp->Config('ydbaijia')->hometitle}{else}{$name}{if $page>'1'}_第{$pagebar.PageNow}页{/if}_{$subname}{/if}</title>
{if $zbp->Config('ydbaijia')->homekeywords}<meta name="Keywords" content="{$zbp->Config('ydbaijia')->homekeywords}" />{/if}
{if $zbp->Config('ydbaijia')->homedescription}<meta name="description" content="{$zbp->Config('ydbaijia')->homedescription}" />{/if}
{elseif $type=='tag'}
<title>{if $tag->Metas->ydbaijia_tagtitle}{$tag.Metas.ydbaijia_tagtitle}{if $page>'1'}_第{$pagebar.PageNow}页{/if}{else}{$tag.Name}_{$name}{if $page>'1'}_第{$pagebar.PageNow}页{/if}_{$subname}{/if}</title>
<meta name="Keywords" content="{if $tag->Metas->ydbaijia_tagkeywords}{$tag.Metas.ydbaijia_tagkeywords}{else}{$tag.Name}{/if}">
{if $tag.Intro || $tag->Metas->ydbaijia_tagdescription}<meta name="description" content="{if $tag->Metas->ydbaijia_tagdescription}{$tag.Metas.ydbaijia_tagdescription}{else}{$tag.Intro}{/if}">{/if}
{elseif $type=='category'}
<title>{if $category->Metas->ydbaijia_catetitle}{$category->Metas->ydbaijia_catetitle}{if $page>'1'}_第{$pagebar.PageNow}页{/if}{else}{$title}_{$name}{/if}</title>
<meta name="Keywords" content="{if $category->Metas->ydbaijia_catekeywords}{$category.Metas.ydbaijia_catekeywords}{else}{$title},{$name}{/if}" />
<meta name="description" content="{if $category->Metas->ydbaijia_catedescription}{$category.Metas.ydbaijia_catedescription}{else}{$title}_{$name}{/if}" />
{else}
<title>{$title}_{$name}</title>
<meta name="Keywords" content="{$title},{$name}" />
<meta name="description" content="{$title}_{$name}{if $page>'1'}_当前是第{$pagebar.PageNow}页{/if}" />
{/if}