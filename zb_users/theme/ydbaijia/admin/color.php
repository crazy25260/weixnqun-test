<?php /* EL PSY CONGROO */    			     
require '../../../../zb_system/function/c_system_base.php';    			   		
require $blogpath . 'zb_users/theme/ydbaijia/admin/header.php';    		    	 
?>
<!--主题配置开始-->
<div class="SubMenu">
<?php ydbaijia_SubMenu(11);?>
</div>
<script type="text/javascript" src="<?php echo $bloghost?>zb_users/theme/ydbaijia/admin/color/farbtastic.js"></script>
<link rel="stylesheet" href="<?php echo $bloghost?>zb_users/theme/ydbaijia/admin/color/farbtastic.css" type="text/css" />
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
    	$('#picker').farbtastic('#color');
		$('#picker2').farbtastic('#color2');
		$('#picker3').farbtastic('#color3');
		$('#picker4').farbtastic('#color4');
	});
</script>
<div id="divMain2">
<!--首页设置-->
	<?php
	if(count($_POST)>0){     		  		 
		$zbp->Config( 'ydbaijia' )->color = $_POST[ 'color' ];       	 	 	
		$zbp->Config( 'ydbaijia' )->color2 = $_POST[ 'color2' ];      	 	 	 
		$zbp->Config( 'ydbaijia' )->color3 = $_POST[ 'color3' ];     						 
		//$zbp->Config( 'ydbaijia' )->color4 = $_POST[ 'color4' ];     			    
     	   	  
		$ydbaijia_css = @file_get_contents($zbp->path.'zb_users/theme/ydbaijia/style/style.css');     	      
		$ydbaijia_css = str_replace("#F2F2F2", $zbp->Config('ydbaijia')->color, $ydbaijia_css);    		 		 	 
		$ydbaijia_css = str_replace("#0047FF", $zbp->Config('ydbaijia')->color2, $ydbaijia_css);       	 			
		$ydbaijia_css = str_replace("#FF6360", $zbp->Config('ydbaijia')->color3, $ydbaijia_css);      	    	
//		if($zbp->Config('ydbaijia')->searchright=='true'){     	  				
//		$ydbaijia_css .='';//自定义css      						
//		}    				 	 	
		@file_put_contents($zbp->path.'zb_users/theme/ydbaijia/style/style.ok.css', $ydbaijia_css);     	 		   
		    	      	
		if(GetVars('coloroff')){//开关       				 
			$zbp->Config('ydbaijia')->coloroff = $_POST['coloroff'];       	 	  
		}else{    			 			 
			$zbp->Config('ydbaijia')->coloroff = '';    	  	 	  
		}    	     		
		$zbp->SaveConfig( 'ydbaijia' );     	  			 
		$zbp->ShowHint( 'good' );    	 					 
	}    		  	   
	?>
	<form id="form2" name="form2" method="post">
		<div class="lbadmin" id="lbcolor">
			<!--///-->
			<h3>主题配色(自定义颜色需开启，否则不要开)</h3>
			<div class="lbimport">
				<span>自定义配色</span>
				<input type="checkbox" name="coloroff" id="coloroff" value="true" <?php if($zbp->Config('ydbaijia')->coloroff) echo 'checked="checked"'?> />
				<i class="red">自定义配色，默认不用开启<br>如需修改CSS文件，请修zb_user/theme/ydbaijia/style/style.css，然后来本页点击下保存</i>
			</div>
			<!--///-->
			<div class="lbimport">
				<span>网站背景色</span>
				<input type="text" name="color" id="color" value="<?php echo $zbp->Config('ydbaijia')->color;?>">
				<div id="picker"></div>
				<i>改变后保存，自行判断</i>
			</div>
			<!--///-->
			<div class="lbimport">
				<span>主色</span>
				<input type="text" name="color2" id="color2" value="<?php echo $zbp->Config('ydbaijia')->color2;?>">
				<div id="picker2"></div>
				<i>改变后保存，自行判断</i>
			</div>
			<!--///-->
			<div class="lbimport">
				<span>辅色</span>
				<input type="text" name="color3" id="color3" value="<?php echo $zbp->Config('ydbaijia')->color3;?>">
				<div id="picker3"></div>
				<i>改变后保存，自行判断</i>
			</div>
			<!--///-->
			<input name="" type="Submit" class="button" value="保存"/>
		</div>
		
	</form>
<!---->
</div>
<?php require $blogpath . 'zb_users/theme/ydbaijia/admin/footer.php'; ?>