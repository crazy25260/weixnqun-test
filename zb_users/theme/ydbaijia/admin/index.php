<?php /* EL PSY CONGROO */    						  
require '../../../../zb_system/function/c_system_base.php';     			 	 	
require $blogpath . 'zb_users/theme/ydbaijia/admin/header.php';     	   		 
?>
<!--主题配置开始-->
<div class="SubMenu">
<?php ydbaijia_SubMenu(3);?>
</div>
<div id="divMain2">
<!--首页设置-->
	<?php
	if(count($_POST)>0){    	 			  	
		$zbp->Config( 'ydbaijia' )->tuijianid = $_POST[ 'tuijianid' ];       					
		$zbp->Config( 'ydbaijia' )->sliderstyle = $_POST[ 'sliderstyle' ];    		 				 
		$zbp->Config( 'ydbaijia' )->hdpid = $_POST[ 'hdpid' ];     			 	  
		    		 	    
		if(GetVars('slider')){//幻灯片    		  	 		
			$zbp->Config('ydbaijia')->slider = $_POST['slider'];    			 	   
		}else{     			 		 
			$zbp->Config('ydbaijia')->slider = '';    		  	 		
		}       		   
		    			 				
		$zbp->SaveConfig( 'ydbaijia' );     	 	    
		$zbp->ShowHint( 'good' );     			  		
	}    		    	 
	?>
	<form id="form2" name="form2" method="post">
		<div class="lbadmin">
			<!--///-->
			<h3>图文调用</h3>
			<!--///-->
			<div class="lbimport">
				<span>幻灯片</span>
				<input type="checkbox" name="slider" id="slider" value="true" <?php if($zbp->Config('ydbaijia')->slider) echo 'checked="checked"'?> />
				<i class="red">请先选择上传方式</i>
			</div>
			
			<div class="lbimport">
				<span>幻灯片上传方式</span>
				<div class="radio">
					<label>
						<input type="radio" id="sliderstyle" name="sliderstyle" value="1" <?php if($zbp->Config('ydbaijia')->sliderstyle == '1') echo 'checked'?> />懒人方法
					</label>
					<label>
						<input type="radio" id="sliderstyle" name="sliderstyle" value="2" <?php if($zbp->Config('ydbaijia')->sliderstyle == '2') echo 'checked'?> />手动上传
					</label>
				</div>
				<i class="red">懒人方法就是直接输入要调用的文章ID，幻灯片调取文章第一张图片；手动请到去上传 <a href="slide.php">幻灯片</a></i>
			</div>
			<div class="lbimport">
				<span>幻灯片懒人方法调用ID</span>
				<input type="text" name="hdpid" id="hdpid" value="<?php echo $zbp->Config('ydbaijia')->hdpid;?>">
				<i class="red">请先选择幻灯片上传方式为“懒人方法”，输入文章ID，多ID英文小逗号隔开</i>
			</div>
			

			<div class="lbimport">
				<span>幻灯右侧图文ID</span>
				<input type="text" name="tuijianid" id="tuijianid" value="<?php echo $zbp->Config('ydbaijia')->tuijianid;?>">
				<i class="red">输入推荐的三个文章ID，多ID英文小逗号隔开<br>注意：需要到 模块管理 - 按着“tuijian”拖拽到默认侧栏才有效，默认显示!</i>
			</div>
			<!--///-->
			<input name="" type="Submit" class="button" value="保存"/>
		</div>
		
	</form>
<!---->
</div>
<?php require $blogpath . 'zb_users/theme/ydbaijia/admin/footer.php'; ?>