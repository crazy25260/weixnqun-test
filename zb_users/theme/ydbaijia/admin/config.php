<?php /* EL PSY CONGROO */      				 	
require '../../../../zb_system/function/c_system_base.php';    	   			 
require $blogpath . 'zb_users/theme/ydbaijia/admin/header.php';    		   	  
?>
<!--主题配置开始-->
<div id="admin-header" class="SubMenu">
	<?php ydbaijia_SubMenu(1);?>
</div>
<div id="divMain2">
<!--基本设置-->
	<?php
	if(count($_POST)>0){        		 	
		$zbp->Config( 'ydbaijia' )->logo = $_POST[ 'logo' ];//logo     		  	  
		//$zbp->Config( 'ydbaijia' )->wechat = $_POST[ 'wechat' ];//wechat    	   	   
		$zbp->Config( 'ydbaijia' )->favicon = $_POST[ 'favicon' ];//favicon    	 			 	 
		$zbp->Config( 'ydbaijia' )->timestyle = $_POST['timestyle'];//timestyle    			 				
		$zbp->Config( 'ydbaijia' )->headhtml = $_POST[ 'headhtml' ];//headhtml    	     		
		$zbp->Config( 'ydbaijia' )->goqrcodeimg = $_POST[ 'goqrcodeimg' ];//goqrcodeimg       			  
		$zbp->Config( 'ydbaijia' )->copyrighthtml = $_POST[ 'copyrighthtml' ];//copyrighthtml     		    	
		    		 	  	 
		if(GetVars('head')){//开关    	 	 			 
			$zbp->Config('ydbaijia')->head = $_POST['head'];    		  			 
		}else{      			   
			$zbp->Config('ydbaijia')->head = '';       			  
		}     		   		
    	 		 			
		if(GetVars('listinfo')){//listinfo       			 	
			$zbp->Config('ydbaijia')->listinfo = $_POST['listinfo'];       				 
		}else{    				  		
			$zbp->Config('ydbaijia')->listinfo = '';    		 	 			
		}     	 	  		
		if(GetVars('postinfo')){//postinfo      		  		
			$zbp->Config('ydbaijia')->postinfo = $_POST['postinfo'];    	    	 	
		}else{       					
			$zbp->Config('ydbaijia')->postinfo = '';    	 	 		 	
		}    	 		    
      		 	  
		    	 	 	  	
		if(GetVars('asidegs')){//开关      		 		 
			$zbp->Config('ydbaijia')->asidegs = $_POST['asidegs'];    	 	   	 
		}else{     	 	 	 	
			$zbp->Config('ydbaijia')->asidegs = '';    	   		  
		}    				   	
		       			  
		if(GetVars('login')){//开关    		 	  		
			$zbp->Config('ydbaijia')->login = $_POST['login'];    	  	 	 	
		}else{       	  		
			$zbp->Config('ydbaijia')->login = '';     							
		}     			  	 
		$zbp->Config('ydbaijia')->zcurl = $_POST['zcurl'];       		 		
		$zbp->Config('ydbaijia')->tgurl = $_POST['tgurl'];     		  	 	
     			  	 
		     	    	 
		if(GetVars('liststyle')){//开关    	  				 
			$zbp->Config('ydbaijia')->liststyle = $_POST['liststyle'];         			
		}else{     	    	 
			$zbp->Config('ydbaijia')->liststyle = '';    			 		 	
		}     	 	  	 
     		   		
		if(GetVars('copyrightoff')){//开关    				 	 	
			$zbp->Config('ydbaijia')->copyrightoff = $_POST['copyrightoff'];     	 	  	 
		}else{     		   		
			$zbp->Config('ydbaijia')->copyrightoff = '';     			 	 	
		}    						 	
		    		  	   
		if(GetVars('share')){//开关    	 	 		  
			$zbp->Config('ydbaijia')->share = $_POST['share'];     	  	 	 
		}else{    	      	
			$zbp->Config('ydbaijia')->share = '';     		   		
		}       	  		
		if(GetVars('related')){//开关    			  			
			$zbp->Config('ydbaijia')->related = $_POST['related'];     	 	 	 	
		}else{    				   	
			$zbp->Config('ydbaijia')->related = '';    	 		 			
		}      		 		 
		if(GetVars('relatedstyle')){//开关    	    			
			$zbp->Config('ydbaijia')->relatedstyle = $_POST['relatedstyle'];     			    
		}else{    					 		
			$zbp->Config('ydbaijia')->relatedstyle = '';    	    	  
		}    			  			
		$zbp->Config('ydbaijia')->relatednum = $_POST['relatednum'];    	 	   		
     	     	
		//     	 					
		if(GetVars('wxjz')){//开关    	   		  
			$zbp->Config('ydbaijia')->wxjz = $_POST['wxjz'];    		 				 
		}else{    	  	   	
			$zbp->Config('ydbaijia')->wxjz = '';      	 		  
		}     			   	
		if(GetVars('target')){//开关       	 	  
			$zbp->Config('ydbaijia')->target = $_POST['target'];     			  		
		}else{    		 	 			
			$zbp->Config('ydbaijia')->target = '';         	  
		}    	  	 			
		     		  		 
		//返回顶部    	 	  		 
		if(GetVars('gotop')){//gotop      			 	 
			$zbp->Config('ydbaijia')->gotop = $_POST['gotop'];    		  	   
		}else{     	 	 			
			$zbp->Config('ydbaijia')->gotop = '';    			  			
		}     	 			  
		if(GetVars('goqrcode')){//goqrcode    		   		 
			$zbp->Config('ydbaijia')->goqrcode = $_POST['goqrcode'];         	  
		}else{     	 	   	
			$zbp->Config('ydbaijia')->goqrcode = '';    		 	  	 
		}     	   			
		if(GetVars('gocomment')){//gocomment    		 		   
			$zbp->Config('ydbaijia')->gocomment = $_POST['gocomment'];    					  	
		}else{         			
			$zbp->Config('ydbaijia')->gocomment = '';    	 	 		 	
		}      					 
		    			   		
		if(GetVars('ydbaijia_clearSetting')){//清空配置    		     	
			$zbp->Config('ydbaijia')->ydbaijia_clearSetting = $_POST['ydbaijia_clearSetting'];    	 	 				
		}else{    	 		    
			$zbp->Config('ydbaijia')->ydbaijia_clearSetting = '';    	  			 	
		}     		  			
		    	 	 	 		
		      	 		  
		$zbp->SaveConfig( 'ydbaijia' );     			    
		$zbp->ShowHint( 'good' );     	  				
	}    	  	 		 
	?>
	<form id="form2" name="form2" method="post">
		<div class="lbadmin">
			<h3>图片上传</h3>
			<!--///-->
			<div class="lbimport">
				<span>LOGO上传</span>
				<input type="text" name="logo" id="logo" class="uplod_img" placeholder="点我！点我！！点我！！！" value="<?php echo $zbp->Config('ydbaijia')->logo;?>">
				<i class="red">logo图片高度50px最佳</i>
			</div>
			<!--///-->

			<div class="lbimport">
				<span>favicon</span>
				<input type="text" name="favicon" id="favicon" class="uplod_img" placeholder="点我！点我！！点我！！！" value="<?php echo $zbp->Config('ydbaijia')->favicon;?>">
				<i>打开网站后，浏览器上显示的小图标是也</i>
			</div>
			
			<!--///-->
			<h3>头部设置</h3>
			<div class="lbimport">
				<span>登陆/注册</span>
				<input type="checkbox" name="login" value="true" <?php if($zbp->Config('ydbaijia')->login) echo 'checked="checked"'?> />
				<i>可关闭</i>
			</div>
			<p style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;注册、投稿功能，需使用用户中心插件中获取，比如 用户中心（百搭）</p>
			<div class="lbimport">
				<span>注册网址</span>
				<input type="text" name="zcurl" placeholder="留空则不显示注册" value="<?php echo $zbp->Config('ydbaijia')->zcurl;?>">
				<i>留空则不显示注册</i>
			</div>
			<div class="lbimport">
				<span>投稿网址</span>
				<input type="text" name="tgurl" placeholder="留空则不显示投稿" value="<?php echo $zbp->Config('ydbaijia')->tgurl;?>">
				<i>留空则不显示投稿</i>
			</div>

			<h3>首页/分类列表样式</h3>
			<div class="lbimport">
				<span>文章列表样式</span>
				<div class="radio">
					<label>
						<input type="radio" name="liststyle" value="1" <?php if($zbp->Config('ydbaijia')->liststyle == '1') echo 'checked'?> />多图版（最高达4图版）
					</label>
					<label>
						<input type="radio" name="liststyle" value="2" <?php if($zbp->Config('ydbaijia')->liststyle == '2') echo 'checked'?> />简单版（单图/无图版）
					</label>
				</div>
			</div>

			
			<h3>返回顶部</h3>
			<div class="lbimport">
				<span>是否开启</span>
				<input type="checkbox" name="gotop" id="gotop" value="true" <?php if($zbp->Config('ydbaijia')->gotop) echo 'checked="checked"'?> />
				<i>可关闭模板自带，去使用返回顶部插件！</i>
			</div>
			<div class="lbimport">
				<span>二维码</span>
				<div class="sub">
					<span>开关二维码</span>
					<input type="checkbox" name="goqrcode" id="goqrcode" value="true" <?php if($zbp->Config('ydbaijia')->goqrcode) echo 'checked="checked"'?> /><br><br>
					<span>上传二维码</span>
					<input type="text" name="goqrcodeimg" id="goqrcodeimg" class="uplod_img" placeholder="点我！点我！！点我！！！" value="<?php echo $zbp->Config('ydbaijia')->goqrcodeimg;?>">
				</div>
				<i>可以关闭掉二维码功能</i>
			</div>
			<div class="lbimport">
				<span>评论快捷</span>
				<input type="checkbox" name="gocomment" id="gocomment" value="true" <?php if($zbp->Config('ydbaijia')->gocomment) echo 'checked="checked"'?> />
				<i>返回顶部文章页的第三个快捷回复</i>
			</div>
			
			
			<!--///-->
			<h3>时间格式</h3>
			<div class="lbimport">
				<span>所有文章时间格式</span>
				<div class="radio">
					<label>
						<input type="radio" id="timestyle" name="timestyle" value="1" <?php if($zbp->Config('ydbaijia')->timestyle == '1') echo 'checked'?> />传统格式
					</label>
					<label>
						<input type="radio" id="timestyle" name="timestyle" value="2" <?php if($zbp->Config('ydbaijia')->timestyle == '2') echo 'checked'?> />"多久前"格式
					</label>
				</div>
			</div>
			<!--///-->
			<h3>作者 分类 时间信息区</h3>
			<div class="lbimport">
				<span>文章列表信息栏</span>
				<input type="checkbox" name="listinfo" id="listinfo" value="true" <?php if($zbp->Config('ydbaijia')->listinfo) echo 'checked="checked"'?> />
				<i>首页、分类文章列表栏中标题下方的作者、时间、阅读等信息</i>
			</div>
			<div class="lbimport">
				<span>文章页信息栏</span>
				<input type="checkbox" name="postinfo" id="postinfo" value="true" <?php if($zbp->Config('ydbaijia')->postinfo) echo 'checked="checked"'?> />
				<i>文章页标题下方的作者、时间、阅读等信息</i>
			</div>
			<!--///-->
			<h3>文章页设置</h3>
			<div class="lbimport">
				<span>版权声明</span>
				<input type="checkbox" name="copyrightoff" id="copyrightoff" value="true" <?php if($zbp->Config('ydbaijia')->copyrightoff) echo 'checked="checked"'?> />
			</div>
			<div class="lbimport">
				<span>&nbsp;</span>
				<textarea type="text" name="copyrighthtml" id="copyrighthtml" rows="3"><?php echo $zbp->Config('ydbaijia')->copyrighthtml;?></textarea>
				<i class="red">文字换行需要用到&lt;br&gt;</i>
			</div>
			<div class="lbimport">
				<span>分享图标+二维码</span>
				<input type="checkbox" name="share" id="share" value="true" <?php if($zbp->Config('ydbaijia')->share) echo 'checked="checked"'?> />
			</div>
			<div class="lbimport">
				<span>相关文章</span>
				<input type="checkbox" name="related" value="true" <?php if($zbp->Config('ydbaijia')->related) echo 'checked="checked"'?> />
			</div>
			<div class="lbimport">
				<span>相关文章显示样式</span>
				<div class="radio">
					<label>
						<input type="radio" name="relatedstyle" value="1" <?php if($zbp->Config('ydbaijia')->relatedstyle == '1') echo 'checked'?> />多图片样式
					</label>
					<label>
						<input type="radio" name="relatedstyle" value="2" <?php if($zbp->Config('ydbaijia')->relatedstyle == '2') echo 'checked'?> />纯标题样式
					</label>
				</div>
			</div>
			<div class="lbimport">
				<span>相关文章显示数字</span>
				<input type="text" name="relatednum" style="width:10%;" value="<?php echo $zbp->Config('ydbaijia')->relatednum;?>">
				<i class="red">留空按默认6，直接填写数字</i>
			</div>

			
			<!--///-->
			<h3>其它全局</h3>
			<div class="lbimport">
				<span>文章列表下拉无限加载</span>
				<input type="checkbox" name="wxjz" id="wxjz" value="true" <?php if($zbp->Config('ydbaijia')->wxjz) echo 'checked="checked"'?> />
				<i>开启后，首页、分类等文章列表显示“查看更多”无限下拉加载</i>
			</div>
			<div class="lbimport">
				<span>右侧栏下拉跟随</span>
				<input type="checkbox" name="asidegs" value="true" <?php if($zbp->Config('ydbaijia')->asidegs) echo 'checked="checked"'?> />
				<i>可开启关闭</i>
			</div>
			<div class="lbimport">
				<span>文章列表新窗口打开</span>
				<input type="checkbox" name="target" id="target" value="true" <?php if($zbp->Config('ydbaijia')->target) echo 'checked="checked"'?> />
			</div>
			<!--///-->
			<h3>Head</h3>
			<div class="lbimport">
				<span>Head加入代码</span>
				<input type="checkbox" name="head" id="head" value="true" <?php if($zbp->Config('ydbaijia')->head) echo 'checked="checked"'?> />
				<i class="red">需要在head中加入代码的开启，慎重！</i>
			</div>
			<!--///-->
			<div class="lbimport">
				<span>加入head的代码</span>
				<textarea name="headhtml" type="text" id="headhtml" rows="6"><?php echo $zbp->Config('ydbaijia')->headhtml;?></textarea>
			</div>
			<!--///-->
			
			<div class="lbimport">
				<span>清空主题配置内设置</span>
				<input type="checkbox" name="ydbaijia_clearSetting" id="ydbaijia_clearSetting" value="true" <?php if($zbp->Config('ydbaijia')->ydbaijia_clearSetting) echo 'checked="checked"'?> />
				<i class="red">不要选！不到万不得已不要选这里，否则，切换其它主题后，当前主题的配置将恢复初始状态！不开玩笑的！</i>
			</div>
			<!--///-->

			<input name="" type="Submit" class="button" value="保存"/>
		</div>
	</form>
	<!---->
</div>
<script type="text/javascript" src="<?php echo $bloghost?>zb_users/plugin/UEditor/ueditor.config.php"></script>
<script type="text/javascript" src="<?php echo $bloghost?>zb_users/plugin/UEditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="<?php echo $bloghost?>zb_users/theme/ydbaijia/admin/js/lib.upload.js"></script>
<?php require $blogpath . 'zb_users/theme/ydbaijia/admin/footer.php'; ?>