<?php /* EL PSY CONGROO */    	  			 	
//文章SEO    	 	   	 
function ydbaijia_article_seo() {     	 		 		
	global $zbp, $article;     			   	
	if($article->Type=="0"){    		 	 			
		echo "<script type=\"text/javascript\" src=\"{$zbp->host}zb_users/theme/{$zbp->theme}/admin/js/seo.js\"></script><link href=\"{$zbp->host}zb_users/theme/{$zbp->theme}/admin/css/seo.css\" rel=\"stylesheet\" type=\"text/css\" />";     	 			  
		if ($article->Metas->ydbaijia_articletitle || $article->Metas->ydbaijia_articlekeywords || $article->Metas->ydbaijia_articledescription){     		 		  
			echo '<div class="articleseo articleseo_on">';      		  		
		}else{     	      
			echo '<div class="articleseo">';    		 	  		
		}
		echo '<span>+++++ 本文SEO +++++</span><input type="text" name="meta_ydbaijia_articletitle" placeholder="标题" value="'.htmlspecialchars($article->Metas->ydbaijia_articletitle).'"/>
    	<input type="text" name="meta_ydbaijia_articlekeywords" placeholder="关键词" value="'.htmlspecialchars($article->Metas->ydbaijia_articlekeywords).'"/>
    	<textarea type="text"  name="meta_ydbaijia_articledescription" placeholder="描述文字" rows="3">'.htmlspecialchars($article->Metas->ydbaijia_articledescription).'</textarea>
    	</div>';
	}     				   
};    	    	 	
      		 	  
//分类SEO    	  		 	 
function ydbaijia_cate_seo(){    	 	 			 
    global $zbp,$cate;
	echo '<div id="alias" class="editmod">
       <span class="title">当前分类标题、关键词、描述<font color="#FF0000">(不填写则按主题默认显示,注：此功能为当前模板自带)</font></span><br />
       <strong>标题</strong><br>
       <input type="text" style="width:75%;" name="meta_ydbaijia_catetitle" value="'.htmlspecialchars($cate->Metas->ydbaijia_catetitle).'"/><br>
       <strong>关键词</strong><br>
       <input type="text" style="width:75%;" name="meta_ydbaijia_catekeywords" value="'.htmlspecialchars($cate->Metas->ydbaijia_catekeywords).'"/><br>
       <strong>描述</strong><br>
       <input type="text" style="width:75%;" name="meta_ydbaijia_catedescription" value="'.htmlspecialchars($cate->Metas->ydbaijia_catedescription).'"/>
       </div>';
}    	   			 
//tag SEO    		 	    
function ydbaijia_tag_seo(){    			   	 
    global $zbp,$tag;
	echo '<div id="alias" class="editmod">
       <span class="title">当前TAG标题、关键词、描述<font color="#FF0000">(不填写则按主题默认显示,注：此功能为当前模板自带)</font></span><br />
       <strong>标题</strong><br>
       <input type="text" style="width:75%;" name="meta_ydbaijia_tagtitle" value="'.htmlspecialchars($tag->Metas->ydbaijia_tagtitle).'"/><br>
       <strong>关键词</strong><br>
       <input type="text" style="width:75%;" name="meta_ydbaijia_tagkeywords" value="'.htmlspecialchars($tag->Metas->ydbaijia_tagkeywords).'"/><br>
       <strong>描述</strong><br>
       <input type="text" style="width:75%;" name="meta_ydbaijia_tagdescription" value="'.htmlspecialchars($tag->Metas->ydbaijia_tagdescription).'"/>
       </div>';
}    			 				
       		 		
//幻灯片       	 	  
function ydbaijia_tags_set(&$template){     	 		 		
	global $zbp,$blogversion;      	    	
	if($blogversion>=150101){      	 				
		$array = $zbp->configs['ydbaijia']->GetData();    	   		 	
	}else{      		  		
		$array = $zbp->configs['ydbaijia']->Data;    	 			   
	}       	 	  
	foreach ($array as $key=>$val){      		  	 
			$template->SetTags($key,$val);    							 
	}      	 		 	
}    	  	    
//选择性调用     			 	  
function ydbaijia_Edit_Response()    			 	   
{    	 			  	
	global $zbp,$article;    	   		  
	ydbaijia_CustomMeta_Response($article);    					 		
}     	  			 
function ydbaijia_CustomMeta_Response(&$object){    	 	 	 	 
	global $zbp;    	  	 	  
	$array=array('lbMeta');     	     	
	$lbMeta_intro = '其他选项';      	 	   
	if(is_array($array)==false)return null;    					 	 
	if(count($array)==0)return null;     		 			 
	foreach ($array as $key => $value) {      		 			
		if($key==0) {     	 			 	
			$single_meta_intro = $lbMeta_intro;      			  	
		}     		   	 
		if(!$single_meta_intro)$single_meta_intro='Metas.' . $value;    						  
		if ($value=='lbMeta') {    			  	  
			$single_meta_option='特荐|头条|推荐|版图|版头|版推';    					 	 
			$ar=explode('|',$single_meta_option);    				    
			echo '<div style="padding:0 0 5px 0; margin:0 0 10px 0;"><laber  class="editinputname">'. $single_meta_intro .'</laber><br />';      					 
			if(!is_array($object->Metas->$value))$object->Metas->$value=array();      	  	 	
			foreach ($ar as $r) {       		 	 
				echo '<label><input name="meta_' . $value . '[]" value="'.htmlspecialchars($r).'" type="checkbox" '.(in_array($r,$object->Metas->$value)?' checked="checked"':'').'/> '.$r.'</label> ';     	  		 	
				if($r=="推荐" || $r=="版推"){     	   	 	
					echo "<br />";    	  					
				}      	  		 
			}     	 		   
			echo '<label onclick="$(&quot;:checkbox[name=\'meta_' . $value . '[]\']&quot;).removeProp(&quot;checked&quot;);$(&quot;:text[name=\'meta_' . $value . '\']&quot;).prop(&quot;disabled&quot;, false);"><input type="text" name="meta_' . $value . '" value="" disabled="disabled" style="display:none;"/>【全不选】<label>';      		  		
			echo '</div>';    	 		 		 
		}    	   	 		
	}     					  
}    	  	  	 
?>